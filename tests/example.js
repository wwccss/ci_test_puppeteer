const puppeteer = require('puppeteer');
const { assert, expect } = require('chai');

describe("sample tests", () => {
    it("test baidu", async() => {
        const browser = await puppeteer.launch({headless: false});
        const page = await browser.newPage();
        await page.goto('https://baidu.com');
        // await page.screenshot({path: 'baidu.png'});

        let value = await page.$eval('#su',el => el.value)
        console.log(value)

        await expect(value).to.be.equal('百度一下');

        await browser.close();
    })
});